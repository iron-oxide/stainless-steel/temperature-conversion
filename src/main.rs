use std::io;
use std::io::Write;

fn main() {

    print!("Enter a fahrenheit temperature: ");
    io::stdout().flush().unwrap();

    let mut fahrenheit = String::new();
    io::stdin().read_line(&mut fahrenheit)
        .expect("Failed to read line");

    let fahrenheit: f64 = match fahrenheit.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            println!("Input must be a number");
            panic!()
        }
    };

    let celsius: f64 = (fahrenheit - 32.0) * 5.0 / 9.0;
    println!("Celsius: {}", celsius);
}
